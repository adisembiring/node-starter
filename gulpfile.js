const gulp = require('gulp');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');
const teamcity = require('eslint-teamcity');


gulp.task('teamcity-lint', () =>
  gulp.src(['**/*.js', '!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format(teamcity))
);

gulp.task('lint', () =>
  gulp.src(['**/*.js', '!node_modules/**'])
    .pipe(eslint())
    .pipe(eslint.format())
);

gulp.task('test', () => {
  process.env.NODE_ENV = 'test';
  return gulp.src(['test/**/*.js'], {
    read: false,
  })
  .pipe(mocha({
    reporter: 'spec',
  }));
});

gulp.task('default', ['lint'], () => {
});
