/* eslint-disable prefer-arrow-callback, func-names, no-unused-expressions */
const expect = require('chai').expect;
const config = require('config');

describe('Document', () => {
  it('should overide config', () => {
    const dbConfig = config.get('Customer.dbConfig');
    expect(dbConfig).is.not.empty;
  });

  it('should able to read config from argument', () => {
    const initialDays = config.get('Customer.credit.initialDays');
    console.log('node-enviroment', process.env.NODE_ENV);
    console.log('node config from process', process.env.NODE_CONFIG);
    console.log(initialDays);
    console.log('initial delays', initialDays);
  });
});


